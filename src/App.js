import { Routes, Route } from "react-router-dom";
import Footer from "./components/Footer/Footer";
import Create from "./pages/create/Create";
import Home from "./pages/home/Home";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="add-product" element={<Create />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
