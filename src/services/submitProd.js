import axios from "axios";

export default function submitProd(data) {
  return axios
    .post(
      "https://scandiwebjrtest.000webhostapp.com/App/index.php/newProduct",
      data
    )
    .then((r) => r.data);
}
