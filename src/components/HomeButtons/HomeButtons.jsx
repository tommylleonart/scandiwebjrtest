import React from "react";
import { Link } from "react-router-dom";
import borrar from "../../services/borrar";
import getAll from "../../services/getAll";
import "./HomeBS.scss";

function HomeButtons({ deletes, setProducts }) {
  const handleDelete = (e) => {
    borrar(deletes);
    getAll().then((r) => setProducts(r));
  };

  return (
    <div className="mainDiv">
      <Link to="/add-product">
        <button className="green">ADD</button>
      </Link>
      <button
        onClick={handleDelete}
        className="red"
        type="submit"
        id="delete-product-btn"
      >
        MASS DELETE
      </button>
    </div>
  );
}

export default HomeButtons;
