import React, { useEffect } from "react";
import getAll from "../../services/getAll";
import Cards from "../Cards/Cards";
import "./MainContS.scss";

function MainContainer({ setDeletes, products, setProducts }) {
  useEffect(() => {
    getAll().then((r) => setProducts(r));
  }, [setProducts, products]);

  return (
    <div className="mainCon">
      {products.map((p) => {
        return (
          <Cards
            name={p.name}
            sku={p.sku}
            type={p.type}
            price={p.price}
            attr={p.attribute}
            key={p.sku}
            setDeletes={setDeletes}
          />
        );
      })}
    </div>
  );
}

export default MainContainer;
