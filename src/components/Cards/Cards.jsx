import React from "react";
import "./CardsS.scss";
import placeholder from "../../placeholder.png";

function Cards({ sku, name, price, attr, setDeletes }) {
  const handleChange = (e) => {
    const { value, checked } = e.target;

    if (checked) {
      setDeletes((prevDeletes) => {
        return {
          ...prevDeletes,
          skus: [...prevDeletes.skus, value],
        };
      });
    } else {
      setDeletes((prevDeletes) => {
        return {
          ...prevDeletes,
          skus: prevDeletes.skus.filter((e) => e !== value),
        };
      });
    }
  };

  return (
    <div className="card">
      <input
        type="checkbox"
        onChange={handleChange}
        value={sku}
        className="delete-checkbox"
      />
      <img src={placeholder} alt="img" className="img" />
      <div className="text">
        <p>{sku}</p>
        <h4>{name}</h4>
        <p>${price}</p>
        <p>{attr}</p>
      </div>
    </div>
  );
}

export default Cards;
