import React, { useState } from "react";
import "./FormS.scss";
function Form({ setProduct }) {
  const [type, setType] = useState("");

  const handleSelect = (e) => {
    setType(e.target.value);
    setProduct((prevProd) => {
      return {
        ...prevProd,
        type: e.target.value,
      };
    });
  };

  const handleChange = (e) => {
    setProduct((prevProd) => {
      return {
        ...prevProd,
        [e.target.name]: e.target.value,
      };
    });
  };

  return (
    <div className="form">
      <form id="product_form">
        <p>SKU</p>
        <input
          onChange={handleChange}
          type="text"
          id="sku"
          name="sku"
          required
        />
        <p>Name</p>
        <input
          onChange={handleChange}
          type="text"
          id="name"
          name="name"
          required
        />
        <p>Price</p>
        <input
          onChange={handleChange}
          type="number"
          id="price"
          name="price"
          required
        />
        <p>Type Switcher</p>
        <select onChange={handleSelect} id="productType">
          <option>Select Type</option>
          <option name="type">DVD</option>
          <option name="type">Book</option>
          <option name="type">Furniture</option>
        </select>
        {type === "DVD" && (
          <>
            <p>Please provide size in MBs...</p>
            <p>Size</p>
            <input
              onChange={handleChange}
              type="text"
              name="size"
              id="size"
              required
            />
          </>
        )}
        {type === "Furniture" && (
          <>
            <p>Please provide dimensions in SxWxL format...</p>
            <p>Height (CM)</p>
            <input
              type="text"
              id="height"
              onChange={handleChange}
              name="height"
            />
            <p>Width (CM)</p>
            <input
              type="text"
              id="width"
              onChange={handleChange}
              name="width"
            />
            <p>Length(CM)</p>
            <input
              type="text"
              id="length"
              onChange={handleChange}
              name="length"
            />
          </>
        )}
        {type === "Book" && (
          <>
            <p>Please provide weight in KGs...</p>
            <p>Weight (KG)</p>
            <input
              onChange={handleChange}
              type="text"
              id="weight"
              name="weight"
            />
          </>
        )}
      </form>
    </div>
  );
}

export default Form;
