import React, { useState } from "react";
import "./HomeS.scss";
import MainContainer from "../../components/MainContainer/MainContainer";
import HomeButtons from "../../components/HomeButtons/HomeButtons";

function Home() {
  const [deletes, setDeletes] = useState({ skus: [] });
  const [products, setProducts] = useState([]);
  return (
    <div className="main">
      <header className="header">
        <h3>Product List</h3>
        <HomeButtons deletes={deletes} setProducts={setProducts} />
      </header>
      <MainContainer
        setDeletes={setDeletes}
        products={products}
        setProducts={setProducts}
      />
    </div>
  );
}

export default Home;
