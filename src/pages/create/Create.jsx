import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Form from "../../components/Form/Form";
import submitProd from "../../services/submitProd";
import "./CreateS.scss";

function Create() {
  const [message, setMessage] = useState([]);

  const [product, setProduct] = useState({
    sku: "",
    name: "",
    price: "",
    type: "",
    weight: "",
    size: "",
    height: "",
    width: "",
    length: "",
  });

  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    submitProd(product).then((r) => setMessage(r));
  };

  useEffect(() => {
    if (message[0] === "Product added!") navigate(-1);
  }, [message, navigate]);

  return (
    <div className="main">
      <header className="header">
        <h3>Product Add</h3>
        <div className="buttons">
          <button onClick={handleSubmit} className="green">
            Save
          </button>
          <Link to="/">
            <button onChange={(e) => e.preventDefault} className="red">
              Cancel
            </button>
          </Link>
        </div>
      </header>
      <div className="content">
        <Form setProduct={setProduct} />
        {message.length > 0 && (
          <div>
            {message.map((e) => {
              return e === "Product added!" ? (
                <p className="success">{e}</p>
              ) : (
                <p className="messages">{e}</p>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
}

export default Create;
