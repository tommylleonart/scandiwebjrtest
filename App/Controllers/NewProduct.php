<?php

class NewProduct extends Products
{
    protected $data;
    protected $newProduct;
    private $errors = [];

    public function __construct()
    {

        $this->data = json_decode(file_get_contents("php://input"));
        $this->controller = $this->data->type;
        $this->type($this->controller) ? $this->newProduct = new $this->controller($this->data) : die(json_encode(["Please select a type..."]));

        $this->validate($this->newProduct);
    }

    public function validate(Validations $prod)
    {
        if (!$prod->sku()) {
            array_push($this->errors, "Invalid SKU...");
        }
        if (!$prod->name()) {
            array_push($this->errors, "Invalid name...");
        }
        if (!$prod->price()) {
            array_push($this->errors, "Invalid price...");
        }
        if (!$prod->validateAttribute()) {
            array_push($this->errors, "Invalid attribute...");
        }
        if (count($this->errors)) {
            die(json_encode($this->errors));
        } else {
            $this->newProduct($this->newProduct);
            echo json_encode(["Product added!"]);
        }
    }
}
