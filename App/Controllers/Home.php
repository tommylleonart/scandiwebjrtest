<?php

class Home extends Products
{
    public function __construct()
    {
        try {
            $products = $this->getAll();
            echo json_encode($products);
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
        }
    }
}
