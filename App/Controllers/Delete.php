<?php

class Delete extends Products
{
    public function __construct()
    {
        $data = json_decode(file_get_contents("php://input"));

        try {
            $this->delete($data);
        } catch (Exception $e) {
            echo json_encode($e->getMessage());
        }
    }
}
