<?php

class Furniture extends Products implements Validations
{
    protected $data;

    function __construct($data)
    {
        $this->data = $data;

        $this->sku = $data->sku;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->type = $data->type;

        return $this->data;
    }

    public function validateAttribute()
    {
        if (is_numeric($this->data->height) && is_numeric($this->data->width) && is_numeric($this->data->length) && floatval($this->data->height > 0) && floatval($this->data->width > 0) && floatval($this->data->length > 0)) {
            $this->attribute = $this->data->height . 'x' . $this->data->width . 'x' . $this->data->length;
            return true;
        }
        return false;
    }
}
