<?php

class DVD extends Products implements Validations
{
    protected $data;

    function __construct($data)
    {
        $this->data = $data;

        $this->sku = $data->sku;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->type = $data->type;

        return $this->data;
    }

    public function validateAttribute()
    {
        if (is_numeric($this->data->size) && floatval($this->data->size > 0)) {
            $this->attribute = $this->data->size . 'MB';
            return true;
        }
        return false;
    }
}
