<?php

class Book extends Products implements Validations
{
    protected $data;

    function __construct($data)
    {
        $this->data = $data;
        $this->sku = $data->sku;
        $this->name = $data->name;
        $this->price = $data->price;
        $this->type = $data->type;

        return $this->data;
    }

    public function validateAttribute()
    {
        if (is_numeric($this->data->weight) && floatval($this->data->weight > 0)) {
            $this->attribute = $this->data->weight . 'Kg';
            return true;
        }
        return false;
    }
}
