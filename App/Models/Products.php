<?php

abstract class Products
{
    protected $data;
    protected $controller;
    protected $sku;
    protected $price;

    public function newProduct($newprod)
    {
        $response = Model::prepare("INSERT INTO `prods` (`sku`, `name`, `price`, `type`, `attribute`) VALUES ('$newprod->sku', '$newprod->name', '$newprod->price', '$newprod->type','$newprod->attribute')");
        return $response->execute();
    }

    public function getAll()
    {
        try {
            $response = Model::prepare("SELECT * FROM prods");
            $response->execute();
            return $response->fetchAll();
        } catch (PDOException $e) {
            echo json_encode($e->getMessage());
        }
    }

    public function getOne($sku)
    {
        $response = Model::prepare("SELECT * FROM prods WHERE sku = '$sku'");
        $response->execute();
        return $response->fetch();
    }

    public function delete($skusObj)
    {
        $skus = $skusObj->skus;
        foreach ($skus as $sku) {
            try {
                $response = Model::prepare("DELETE FROM `prods` WHERE `sku` = '$sku'");
                $response->execute(array(":sku" => $sku));
            } catch (Exception $e) {
                echo json_encode($e->getMessage());
            }
        }
    }

    public function sku()
    {
        return (!preg_match('/\s/', $this->sku) && !$this->getOne($this->sku) && (strlen($this->sku) > 0));
    }

    public function name()
    {
        return (strlen($this->name) > 0);
    }

    public function price()
    {
        return (floatval($this->price) > 0);
    }
    public function type($prodType)
    {
        return ($prodType !== "" && $prodType !== "Select Type");
    }
}
