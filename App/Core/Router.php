<?php

class Router
{
    protected $routes = [];

    public function __construct()
    {
        $this->routes = array(
            "" => "Home",
            "newProduct" => "NewProduct",
            "delete" => "Delete"
        );

        $controller = $this->getRoute();
        return new $controller;
    }


    public function parseUrl()
    {
        return explode("/", $_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL);
    }

    public function getRoute()
    {
        $url = $this->parseUrl();
        if (isset($url[3]) && array_key_exists($url[3], $this->routes)) {
            return $this->routes[$url[3]];
        } else {

            return $this->routes[""];
        }
    }
}
