<?php

interface Validations
{
    public function sku();
    public function name();
    public function price();
    public function type($prodType);
    public function validateAttribute();
}
