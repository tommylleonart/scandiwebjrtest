<?php

function autoload($class_name)
{
    $paths = array(
        '/Controllers',
        '/Core',
        '/Models',
    );


    foreach ($paths as $path) {
        $file = sprintf('../App/%s/%s.php', $path, $class_name);
        if (is_file($file)) {
            include_once $file;
        }
    }
}

spl_autoload_register('autoload');
